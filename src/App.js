import React from 'react';
import TodoItem from './TodoItem'
import './App.css';
import todoData from "./todoData";
import Menu from './menu'

class TodoList extends React.Component {
  constructor() {
    super();
    this.state = { todoItems: todoData, isInEdit: false };
    this.addItem = this.addItem.bind(this);
    this.clearAll = this.clearAll.bind(this);
    this.setEdit = this.setEdit.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }
  addItem() {
    const newItemId = this.state.todoItems.length + 1;
    let items = this.state.todoItems;
    items.push({ id: newItemId, task: "Enter task here..." })
    this.setState({ todoItems: items })
  }
  clearAll() {
    this.setState({ todoItems: [] })
  }
  setEdit() {
    const currentEditStatus = this.state.isInEdit;
    this.setState({ isInEdit: !currentEditStatus });
  }
  deleteItem(id) {
    let items = [...this.state.todoItems];
    for (let i = 0; i < items.length; i++)
      if (items[i].id === id)
        items.splice(i, 1)
    this.setState({ todoItems: items })
  }

  render() {
    const todoItems = this.state.todoItems.map(entry => <TodoItem
      key={entry.id}
      id={entry.id}
      task={entry.task}
      isEdit={this.state.isInEdit}
      onDelete={this.deleteItem}
    />)

    return (
      <div className="TodoList">
        <Menu
          addItemCallback={this.addItem}
          setEditCallback={this.setEdit}
          clearAllCallback={this.clearAll}
          isEdit={this.state.isInEdit}>
        </Menu>
        {todoItems}
      </div>
    );
  }
}

export default TodoList;
