import React from 'react';
import ContentEditable from "react-contenteditable";
import Button from 'react-bootstrap/Button'
class TodoItem extends React.Component {
    constructor(props) {
        super();
        this.state = { isDone: false, task: props.task }
        this.updateStatus = this.updateStatus.bind(this)
    }
    updateStatus() {
        const currentStatus = this.state.isDone
        this.setState({ isDone: !currentStatus })
    }
    handleChange = evt => {
        this.setState({ task: evt.target.value });
    };

    render() {
        return (

            <div className='TodoItem'>
                <input type='checkbox' checked={this.state.isDone} onChange={this.updateStatus}></input>
                <ContentEditable innerRef={this.contentEditable}
                    html={this.state.task}
                    disabled={false}
                    onChange={this.handleChange}
                    className={'task ' + ((this.state.isDone) ? "done" : "notDone")}
                    style={{ resize: 'none', width: '50%' }}
                />
                <Button
                    className="deleteBtn"
                    style={((!this.props.isEdit) ? { display: "none" } : { display: 'inline-flex' })}
                    variant='danger'
                    onClick={() => { this.props.onDelete(this.props.id) }}><strong>X</strong>
                </Button>


            </div >

        )
    }
}


export default TodoItem;