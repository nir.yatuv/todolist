import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TodoList from './App';

ReactDOM.render(
  <React.StrictMode>
    <TodoList />
    <footer>© 2020 Nir Yatuv</footer>
  </React.StrictMode>,
  document.getElementById('root')
);


