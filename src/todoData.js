const todoData = [
    {
        id: 1,
        task: "Pickup Groceries"
    },
    {
        id: 2,
        task: "Do Homework"
    },
    {
        id: 3,
        task: "Wash Dishes"
    }
]

export default todoData;