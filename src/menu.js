import React from 'react';
import Button from 'react-bootstrap/Button'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import './App.css';
class Menu extends React.Component {

    render() {
        let removeButtonText = 'Remove X';
        let removeButtonVariant = 'danger';
        if (this.props.isEdit) {
            removeButtonText = 'Apply ✔';
            removeButtonVariant = 'primary'
        }

        return (
            <ButtonGroup>
                <Button className="listBtn" variant='success' onClick={this.props.addItemCallback}>Add <strong>+</strong></Button>
                <Button className="listBtn" variant={removeButtonVariant} onClick={this.props.setEditCallback}>{removeButtonText}</Button>
                <Button className="listBtn" variant='warning' onClick={this.props.clearAllCallback}>Clear All</Button>
            </ButtonGroup>
        )
    }
}
export default Menu;